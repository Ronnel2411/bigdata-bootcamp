//Entrada de Datos
d3.json('practica_airbnb.json').then((FeatureCollection)=>{
    console.log(FeatureCollection);
    drawMap(FeatureCollection)
    });


function drawMap(FeatureCollection){
    //Declaracion de tamaño del lienzo
    var height = 800;
    var width = 800;
    var widthTooltip = 400;
    var heightTooltip = 300;
    var widthTooltipGrafico = 350;
    var heightTooltipGrafico = 250;
    var marginbottom = 100;
    var margintop = 50;
    var marginright = 100;
    var posXinnerTooltip=40;
    var posYinnerTooltip=25;
    var yTicks=10;


    var svg = d3.select('div')
        //.attr("class", "svg-container")
        .append('svg')
        //.attr("preserveAspectRatio", "xMinYMin meet")
        //.attr("viewBox", `0 0 ${(width+marginright)} ${(height+ marginbottom + margintop)}`)
        //.attr("class", " svg-content")
        .attr('width', width)
        .attr('height', height + marginbottom + margintop)
        .append("g")
        //.attr("transform", "translate(" + marginright + "," + margintop + ")");
    
    // Calcular el centro de las coordenadas
    var centroMadrid = d3.geoCentroid(FeatureCollection);
    //console.log("centro " + centroMadrid)


    // proyeccion de cordenadas a pixeles
    var proyeccion = d3.geoMercator()
        .fitSize([width,height],FeatureCollection)
        //.scale(80000)
        //.center(centroMadrid)
        //.translate([width/2 , height/2]);

    //console.log(proyeccion([-3.5, 40.51]));

    //Calculamos el precio maximo y minimo para usar dentro de nuestra paleta de colores
    var precioMax = d3.max(FeatureCollection.features, function(d){
        //console.log(d.properties.avgprice);        
        return d.properties.avgprice
        
    });
    var precioMin = d3.min(FeatureCollection.features, function(d){
        return d.properties.avgprice
        
    })
    //Calculamos el rango de precio que estara dentro de cada color de la paleta de colores
    //Se usaran 10 colores

    var coloresEnPaleta = 10;    
    var rangoTotal = precioMax - precioMin;
    var rangoColor = rangoTotal/ coloresEnPaleta ; //10 colores en la paleta


    //console.log("precio max = " + precioMax);
    //console.log("precio min = " + precioMin);
    //console.log("rango de precios = " + rangoTotal);
    //console.log("rango de precio por color = " + rangoColor);

    // Funcion que genera el mapa
    var genradorDeMapa = d3.geoPath().projection(proyeccion);

    var escalaColores = d3.scaleOrdinal()
        .range(d3.schemeTableau10);
    //console.log("escala de colores = " + d3.schemeTableau10[0])

    // Generador del mapa
    var Mapa = svg.append("g")
        .selectAll("path")
        .data(FeatureCollection.features)
        .enter()
        .append("path")
        .attr("d",function(d){
            return genradorDeMapa(d)
        })
        .attr("fill", function (d, i){
            var indiceColor = 0;
            var idColor = 0;
                //console.log("f inicial : "+indiceColor);
                //console.log("precio promedio " + d.properties.avgprice);
                while (indiceColor <= (coloresEnPaleta-1)){
                    //console.log(indiceColor);                    
                    if (d.properties.avgprice >= precioMin+(rangoColor*indiceColor)){
                        //console.log("Es mayor" + " color es " + indiceColor)
                        idColor = indiceColor
                    } 
                    indiceColor++;
                }
                //console.log("Color de el bariros es " + idColor)
            return d3.schemeTableau10[idColor];         
        });

    //Texto Mapa de Madrid


    //Leyenda de colores

    var widthRect = width/coloresEnPaleta;
    var heightRect = 20;
    var leyenda = svg.append("g")
        .selectAll("rect")
        .data(d3.schemeTableau10)
        .enter()
        .append("rect")
        .attr("x", function(d,i){
            return widthRect*i
        })
        .attr("y", 0)
        .attr("width", widthRect)
        .attr("height", heightRect)
        .attr("fill", (d)=>d);

    var textoLeyenda = svg.append("g")
        .selectAll("text")
        .data(d3.schemeTableau10)
        .enter()
        .append("text")
        .attr("x", function(d,i){
            return (widthRect*i)+ (widthRect/10)
        })
        .attr("y", heightRect/1.5)
        .attr("font-size",12)
        .text(function(d,i){
            console.log(i)
            var desde = precioMin+(i*rangoColor);
            var hasta = desde + rangoColor ;
            //console.log(desde)
            //console.log(hasta)
            return desde + "$ - " + hasta +"$" ;
        })

        var MapaText="MAPA DE MADRID";
        var yMapaTextElementos = MapaText.length;
        var indiceYMapaText=0;
    
        console.log("elementos en y " + yMapaTextElementos);
    
        while(indiceYMapaText <= yMapaTextElementos){
            var tituloYaxis = svg.append("g")
            .append("text")  
            .attr("x",10)
            .attr("y",300+(indiceYMapaText*25))
            .attr("font-size",30)
            .attr("fill","red")
            .text(MapaText.charAt(indiceYMapaText));
            indiceYMapaText ++;
        };
    


    // Minigrafico propiedades x habitacion
    Mapa.on("mouseover", function(event,d){
        d3.select(this)
            .transition()
            .duration(1000)
            .attr("opacity",0)

            tooltip
            .style("visibility","visible")
            .style("left", event.pageX + "px")
            .style("top", event.pageY + "px");

            //Variables maximas y minimas ambos ejes
            console.log(d.properties.properties)
            var xMax = d3.max(d.properties.properties, (d) => d.bedrooms);
            var xMin = 0;
            var yMax = d3.max(d.properties.properties, function(d,i){
                return i;
            });
            var yMin = 0;     
            
            //Fondo Gris
            var background = tooltip.append("g")
            .append("rect")
            .attr("width",widthTooltipGrafico)
            .attr("height",heightTooltipGrafico)
            .attr("x",posXinnerTooltip)
            .attr("y",posYinnerTooltip)
            .attr("fill","#D3D3D3");

            // Escalas de ejes
            var escalaX = d3.scaleLinear()
            .domain([0,xMax ])
            .range([posXinnerTooltip,widthTooltipGrafico+posXinnerTooltip]);

            console.log("Escala maxima: " + escalaX(xMin));
            console.log("Escala minima: " + escalaX(xMax));
        
            var escalaY = d3.scaleLinear()
            .domain([yMax,0])
            .range([posYinnerTooltip,heightTooltipGrafico+posYinnerTooltip]);

            var xAxis = d3.axisBottom(escalaX).ticks(xMax);
            var yAxis = d3.axisLeft(escalaY).ticks(yTicks);    
  
            //Linea Guias
            var guiaY = tooltip.append("g")
                .selectAll("rect")
                .data(d.properties.properties)
                .enter()
                .append("rect")
                .attr("width",1)
                .attr("height",heightTooltipGrafico)
                .attr("x",function(d){
                    return escalaX(d.bedrooms)
                    
                })
                .attr("y",posYinnerTooltip)
                .attr("fill","white");   
            
            var guiaX = tooltip.append("g") 
                .selectAll("rect")
                .data(d.properties.avgbedrooms)
                .enter()       
                .append("rect")
                .attr("width",widthTooltipGrafico)
                .attr("height",1)
                .attr("x",posXinnerTooltip)
                .attr("y",function(d){
                    return escalaY(d.total)   
                })
                .attr("fill","white");

        
            //Visualizacion de Axis
            var visualXaxis = tooltip.append("g")
            .attr("transform", "translate(0," + (heightTooltipGrafico+posYinnerTooltip) + ")")
            .call(xAxis);
            var visualYaxis = tooltip.append("g")
            .attr("transform", "translate("+posXinnerTooltip + ",0)")
            .call(yAxis);

            //Titulos de Axis
            var tituloXaxis = tooltip.append("g")
                .append("text")  
                .attr("x",150)
                .attr("y",300)
                .attr("font-size",12)
                .text("Amount of Bedrooms");
            
            var yText="PROPERTIES";
            var yTextElementos = yText.length;
            var indiceYText=0;

            while(indiceYText <= yTextElementos){
                var tituloYaxis = tooltip.append("g")
                .append("text")  
                .attr("x",2)
                .attr("y",120+(indiceYText*10))
                .attr("font-size",12)
                .text(yText.charAt(indiceYText));
                indiceYText ++;
            };

            var tituloGraficoPuntos = tooltip.append("g")
                .append("text")  
                .attr("x",100)
                .attr("y",10)
                .attr("font-size",14)
                .attr("fill","green")
                .text("Properties by neighborhood in " + d.properties.name);
            

            console.log(d.properties.avgbedrooms);
            //Creacion de elementos circulos
            var ratio = 5;
            var circle = tooltip.append("g")
                .selectAll("circle")
                .data(d.properties.avgbedrooms)
                .enter()
                .append("circle")
                .attr("cx",function(d){
                    return escalaX(d.bedrooms)
                }).attr("cy",function(d){
                    return escalaY(d.total)
                })
                .attr("r",ratio)
                .attr("fill","blue");
               })
            
 

    Mapa.on("mouseout", function(event,d){
        d3.select(this)
            .transition()
            .duration(500)
            .attr("opacity",1)

        tooltip
            .style("visibility","hidden")
            .style("left", event.pageX + "px")
            .style("top", event.pageY + "px");

        
        var background = tooltip.append("g")
            .append("rect")
            .attr("width",widthTooltip)
            .attr("height",heightTooltip)
            .attr("x",0)
            .attr("y",0)
            .attr("fill","white");
        
    })

    

    var tooltip = d3.select("div")
    .append("svg")
    .attr("width",widthTooltip)
    .attr("height",heightTooltip)
    .style("position","absolute")
    .style("pointer-events","none")
    .style("visibility","hidden")
    .style("background-color","white")
    .style("border","solid")
    .style("border-width","1px")
    .style("border-radius","5px")
    .style("padding","5px");

}


